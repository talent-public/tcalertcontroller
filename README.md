# TCAlertController for iOS

This library helps you manage dialog view in your app

# How to use

## Installation

1. Add this line in your `Podfile`

```
pod 'TCAlertController', :git => 'https://gitlab.com/talent-public/tcalertcontroller.git', :branch => 'main'
```

2. Run this command

```
pod install
```

## Sample code

```Swift
TCAlertController.message(title: "Hello", details: "everyone", image: nil, next: nil)
```
