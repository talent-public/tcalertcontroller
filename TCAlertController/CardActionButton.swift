//
//  CardActionButton.swift
//  TalentModal
//
//  Created by Peerasak Unsakon on 17/11/2563 BE.
//

import UIKit

public class CardActionButton: UIButton {
    
    public enum Style {
        case clear
        case fill
        case black
        case outlined
    }
    
    public enum Color {
        case primary
        case white
        case dark
    }
    
    public var actionInfo: CardButton!
        
    // MARK: - init methods
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    public convenience init(title: String, style: CardActionButton.Style = .fill, actionInfo: CardButton, frame: CGRect = CGRect.zero) {
        self.init(frame: frame)
        
        self.actionInfo = actionInfo
        
        switch style {
        case .clear:
            self.setupUI(style: style, borderColor: UIColor.clear, titleColor: UIColor.black, bgColor: UIColor.clear, font: .systemFont(ofSize: 16, weight: .medium))
            break
        case .fill:
            self.setupUI(style: style, borderColor: .yellow, titleColor: TCAlertController.shared.primaryTextColor, bgColor: TCAlertController.shared.primaryBackgroundColor, font: .systemFont(ofSize: 16, weight: .medium))
            break
        case .black:
            self.setupUI(style: style, borderColor: .black, titleColor: .white, bgColor: .black, font: .systemFont(ofSize: 16, weight: .medium))
            break
        case .outlined:
            self.setupUI(style: style, borderWidth: 1, borderColor: .black, titleColor: .black, bgColor: .white, font: .systemFont(ofSize: 16, weight: .medium))
            break
        }
        
        self.setTitle(title, for: .normal)
    }
    
    public convenience init(title: String, radius: CGFloat = 25, borderWidth: CGFloat = 2, borderColor: UIColor = .white, titleColor: UIColor = .black, bgColor: UIColor = .white, frame: CGRect = CGRect.zero, font: UIFont = UIFont.systemFont(ofSize: 16)) {
        self.init(frame: frame)
        self.setupUI(radius: radius, borderWidth: borderWidth, borderColor: borderColor, titleColor: titleColor, bgColor: bgColor, font: font)
        self.setTitle(title, for: .application)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    private func setupUI(style: Style = .clear, radius: CGFloat = 25, borderWidth: CGFloat = 2, borderColor: UIColor = .white, titleColor: UIColor = .black, bgColor: UIColor = .white, font: UIFont = UIFont.systemFont(ofSize: 16)) {
        
        
        self.layer.cornerRadius = radius

        if style == .outlined {
            self.frame = self.frame.insetBy(dx: -borderWidth, dy: -borderWidth);
            self.layer.borderWidth = borderWidth
            self.layer.borderColor = borderColor.cgColor
        }
        
        if self.layer.cornerRadius > 0 {
            self.layer.masksToBounds = true
        }
        
        self.setTitleColor(titleColor, for: .normal)
        self.setTitleColor(.darkGray, for: .disabled)
        self.titleLabel?.font = font
        
        self.setBackgroundColor(bgColor, for: .normal)
        self.setBackgroundColor(.gray, for: .disabled)
    }
    
}
