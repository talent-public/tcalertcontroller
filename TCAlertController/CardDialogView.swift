//
//  CardDialogView.swift
//  TalentModal
//
//  Created by Peerasak Unsakon on 16/11/2563 BE.
//

import UIKit
import SwiftEntryKit

public class CardDialogView: UIView, DialogView {
    
    @IBOutlet public weak var contentView: UIView!
    @IBOutlet public weak var dialogStackView: UIStackView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var subtitleLabel: UILabel!
    @IBOutlet public weak var detailLabel: UILabel!
    @IBOutlet public weak var buttonPanel: UIStackView!
    
    @IBOutlet public weak var agreementView: UIStackView!
    @IBOutlet public weak var checkboxView: UIButton!
    @IBOutlet public weak var agreementButton: UIButton!
    
    var dialogInfo: DialogDescription?
    
    public convenience init(dialogInfo: DialogDescription, footer: UIView?, frame: CGRect = CGRect.zero) {
        self.init(frame: frame)
        self.initialize(title: dialogInfo.title, actions: dialogInfo.actionPanel)
        
        self.dialogInfo = dialogInfo
        
        if let image = dialogInfo.image {
            self.addImage(image: image)
        }
        
        if let subtitle = dialogInfo.subtitle {
            self.addSubtitle(subtitle)
        }else {
            self.subtitleLabel.isHidden = true
        }
        
        if let details = dialogInfo.detail {
            self.addDetails(detail: details)
        }else {
            self.detailLabel.isHidden = true
        }
        
        self.agreementView.isHidden = !dialogInfo.requiredAgreement
        
        if let footer = footer {
            self.addFooter(view: footer)
        }
        
        guard let cardActions = dialogInfo.actionPanel else { return }
        self.createButtons(actions: cardActions)
    }
    
    public convenience init(title: String?, subtitle: String?, details: String?, image: UIImage?, frame: CGRect = CGRect.zero, actions: CardActions, footer: UIView?) {
        self.init(frame: frame)
        
        self.initialize(title: title, actions: actions)
        
        if let image = image {
            self.addImage(image: image)
        }
        
        if let subtitle = subtitle {
            self.addSubtitle(subtitle)
        }else {
            self.subtitleLabel.isHidden = true
        }
        
        if let details = details {
            self.addDetails(detail: details)
        }else {
            self.detailLabel.isHidden = true
        }
        
        self.agreementView.isHidden = true
        
        if let footer = footer {
            self.addFooter(view: footer)
        }
        
        self.createButtons(actions: actions)
    }
    
    public convenience init(title: String?, subtitle: String?, attributedDetails: NSAttributedString?, image: UIImage?, frame: CGRect = CGRect.zero, actions: CardActions, footer: UIView?) {
        self.init(frame: frame)
        
        self.initialize(title: title, actions: actions)
        
        if let image = image {
            self.addImage(image: image)
        }
        
        if let subtitle = subtitle {
            self.addSubtitle(subtitle)
        }else {
            self.subtitleLabel.isHidden = true
        }
        
        if let attributedDetails = attributedDetails {
            self.addDetails(attributedDetails: attributedDetails)
        }else {
            self.detailLabel.isHidden = true
        }
        
        self.agreementView.isHidden = true
        
        if let footer = footer {
            self.addFooter(view: footer)
        }
        
        self.createButtons(actions: actions)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    public func commonInit() {
        
        TCAlertController.bundle.loadNibNamed(CardDialogView.className, owner: self, options: nil)
        
        self.contentView.layer.cornerRadius = 10
        if self.contentView.layer.cornerRadius > 0 {
            self.contentView.layer.masksToBounds = true
        }
        self.contentView.fitInView(self)
    }
    
    
    func initialize(title: String?, actions: CardActions?) {
        
        if let title = title {
            self.titleLabel.isHidden = false
            self.titleLabel?.font = .boldSystemFont(ofSize: 16)
            self.titleLabel.numberOfLines = 0
            self.titleLabel.text = title
            
        } else {
            self.titleLabel.isHidden = true
        }
        
    }
    
    @IBAction func checkboxTapped(_ sender: Any) {
        self.checkboxView.isSelected = !self.checkboxView.isSelected
        self.updateButtons()
    }
    
    @IBAction func agreementButtonTapped(_ sender: Any) {
        if let block = self.dialogInfo?.agreementHandler {
            block()
        }
    }
    
    private func updateButtons() {
        self.buttonPanel.arrangedSubviews.forEach { (view) in
            if let button = view as? CardActionButton {
                if button.actionInfo.style == .fill {
                    button.isEnabled = self.checkboxView.isSelected
                }
            }
        }
    }
    
    private func createButtons(actions: CardActions) {
        
        self.buttonPanel.axis = actions.alignment
        
        actions.buttons.forEach { (cardButton) in
            let button: CardActionButton = {
                let button = CardActionButton(title: cardButton.title, style: cardButton.style, actionInfo: cardButton)
                button.heightAnchor.constraint(equalToConstant: 50).isActive = true
                button.addAction(for: .touchUpInside) { _ in
                    if let action = cardButton.action {
                        action()
                    }
                }
                
                if let dialogInfo = self.dialogInfo {
                    if cardButton.style == .fill {
                        button.isEnabled = !dialogInfo.requiredAgreement
                    }
                }
                
                return button
            }()
            self.buttonPanel.addArrangedSubview(button)
        }
    }
    
    private func addDetails(detail: String) {
        self.detailLabel.isHidden = false
        self.detailLabel.textAlignment = .center
        self.detailLabel.numberOfLines = 0
        self.detailLabel.text = detail
    }
    
    private func addDetails(attributedDetails: NSAttributedString) {
        self.detailLabel.isHidden = false
        self.detailLabel.textAlignment = .center
        self.detailLabel.numberOfLines = 0
        self.detailLabel.attributedText = attributedDetails
    }
    
    private func addSubtitle(_ subtitle: String) {
        self.subtitleLabel.isHidden = false
        self.subtitleLabel.textAlignment = .center
        self.subtitleLabel.numberOfLines = 0
        self.subtitleLabel.text = subtitle
    }
    
    private func addImage(image: UIImage) {
        let centerImageView: UIImageView = {
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.heightAnchor.constraint(equalToConstant: 70).isActive = true
            return imageView
        }()
        
        self.dialogStackView.insertArrangedSubview(centerImageView, at: 0)
    }
    
    private func addFooter(view: UIView) {
        self.dialogStackView.addArrangedSubview(view)
    }
}

public class PartialCardDialogView: CardDialogView {
    
    @IBOutlet weak var cardView: UIView!
    
    public override func commonInit() {
        
        TCAlertController.bundle.loadNibNamed(PartialCardDialogView.className, owner: self, options: nil)
        
        self.cardView.layer.cornerRadius = 10
        if self.cardView.layer.cornerRadius > 0 {
            self.cardView.layer.masksToBounds = true
        }
        
        self.contentView.fitInView(self)
    }
    
}
