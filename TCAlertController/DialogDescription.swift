//
//  Dialog.swift
//  TalentModal
//
//  Created by Peerasak Unsakon on 16/11/2563 BE.
//

import Foundation
import UIKit
import SwiftEntryKit

// MARK: - Protocol -

public protocol DialogView {
    
}

public protocol DialogDescription {
    var image: UIImage? { get set }
    var title: String? { get set }
    var subtitle: String? { get set }
    var detail: String? { get set }
    var actionPanel: CardActions! { get set }
    var requiredAgreement: Bool { get set }
    var agreementHandler: (() -> Void)? { get set }
}

public protocol CardActions {
    var alignment: NSLayoutConstraint.Axis { get set }
    var buttons: [CardButton] { get set }
}

public protocol CardButton {
    var title: String { get set }
    var style: CardActionButton.Style { get set }
    var action: (() -> Void)? { get set }
}

// MARK: - Structure Model -

public struct DialogActions: CardActions {
    public var alignment: NSLayoutConstraint.Axis
    public var buttons: [CardButton]
    
    public init(alignment: NSLayoutConstraint.Axis, buttons: [CardButton]) {
        self.alignment = alignment
        self.buttons = buttons
    }
}

public struct DialogActionButton: CardButton {
    public var title: String
    public var style: CardActionButton.Style = .fill
    public var color: CardActionButton.Color = .primary
    public var action: (() -> Void)?
    
    public init(title: String, style: CardActionButton.Style = .fill, color: CardActionButton.Color = .primary, action: (() -> Void)? = nil) {
        self.title = title
        self.style = style
        self.color = color
        self.action = action
    }
}

public struct Dialog: DialogDescription {
    
    public var image: UIImage?
    public var title: String?
    public var subtitle: String?
    public var detail: String?
    public var attributedDetail: NSAttributedString? = nil
    public var actionPanel: CardActions!
    public var requiredAgreement: Bool = false
    public var agreementHandler: (() -> Void)?
    
    public init(image: UIImage? = nil, title: String? = nil, subtitle: String? = nil, detail: String? = nil, attributedDetail: NSAttributedString? = nil, actionPanel: CardActions? = nil, requiredAgreement: Bool = false, agreementHandler: (() -> Void)? = nil) {
        self.image = image
        self.title = title
        self.subtitle = subtitle
        self.detail = detail
        self.attributedDetail = attributedDetail
        self.actionPanel = actionPanel
        self.requiredAgreement = requiredAgreement
        self.agreementHandler = agreementHandler
    }
    
}
