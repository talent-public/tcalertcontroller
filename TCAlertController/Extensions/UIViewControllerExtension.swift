//
//  UIViewControllerExtension.swift
//  TCAlertController
//
//  Created by Peerasak Unsakon on 9/12/2563 BE.
//

import UIKit

class ClosureWrapper {
    var close: (() -> Void)?
    var complete: (() -> Void)?

    init(_ close: (() -> Void)? = nil, complete: (() -> Void)? = nil) {
        self.close = close
        self.complete = complete
    }
}

struct AssociatedKeys {
    static var dialogDismissHandler: ClosureWrapper = ClosureWrapper({})
    static var dialogCompletionHandler: ClosureWrapper = ClosureWrapper(complete: {})
}

public protocol TCAlertPresentationProtocol {
    
    var dialogCompletionHandler: (() -> Void)? { get set }
    var dialogDismissHandler: (() -> Void)? { get set }
    
}

extension UIViewController: TCAlertPresentationProtocol {
    
    public var dialogCompletionHandler: (() -> Void)? {
        get {
            if let cl = objc_getAssociatedObject(self, &AssociatedKeys.dialogCompletionHandler) as? ClosureWrapper {
                return cl.complete
            }
            return nil
        }

        set {
            objc_setAssociatedObject(self, &AssociatedKeys.dialogCompletionHandler, ClosureWrapper(complete: newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    public var dialogDismissHandler: (() -> Void)? {
        get {
            if let cl = objc_getAssociatedObject(self, &AssociatedKeys.dialogDismissHandler) as? ClosureWrapper {
                return cl.close
            }
            return nil
        }

        set {
            objc_setAssociatedObject(self, &AssociatedKeys.dialogDismissHandler, ClosureWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    @IBAction func cancelDialogButtonTapped(_ sender: Any?) {
        guard let dismiss = self.dialogDismissHandler else { return }
        dismiss()
    }
    
    @IBAction func completeDialogButtonTapped(_ sender: Any?) {
        guard let complete = self.dialogCompletionHandler else { return }
        complete()
    }
}
