//
//  ModalPreset.swift
//  BNK48
//
//  Created by Peerasak Unsakon on 13/11/2563 BE.
//  Copyright © 2563 BNK48. All rights reserved.
//

import Foundation
import SwiftEntryKit


public struct ModalPreset {
    
    private static var defaultAttributes: EKAttributes {
        get {
            var attributes: EKAttributes
            attributes = .centerFloat
            attributes.displayMode = .inferred
            attributes.displayDuration = .infinity
            attributes.screenBackground = .color(color: EKColor(UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.6)))
            attributes.entryBackground = .color(color: .clear)
            attributes.entryInteraction = .absorbTouches
            attributes.scroll = .disabled
            attributes.roundCorners = .all(radius: 16.0)
            attributes.positionConstraints.verticalOffset = 0
            attributes.positionConstraints.safeArea = .overridden
            attributes.statusBar = .dark
            
            attributes.entranceAnimation = .init(
                translate: .init(
                    duration: 0.3,
                    spring: .init(damping: 1, initialVelocity: 0)
                )
            )
            
            attributes.exitAnimation = .init(
                translate: .init(duration: 0.2)
            )
            
            attributes.popBehavior = .animated(
                animation: .init(
                    translate: .init(duration: 0.2)
                )
            )
            
            return attributes
        }
    }
    
    public static func normal(screenInteraction: EKAttributes.UserInteraction = .dismiss) -> EKAttributes {
        var attributes: EKAttributes = defaultAttributes
        
        attributes.screenInteraction = screenInteraction
        
        attributes.positionConstraints.maxSize = .init(
            width: .constant(value: 320),
            height: .constant(value: 480)
        )
        
        attributes.positionConstraints.size = .init(
            width: .offset(value: 20),
            height: .intrinsic
        )
        
        return attributes
    }
        
    public static func webView(backgroundColor: UIColor, screenInteraction: EKAttributes.UserInteraction = .dismiss) -> EKAttributes {
        var attributes: EKAttributes = defaultAttributes
        
        attributes.screenInteraction = screenInteraction
        attributes.entryBackground = .color(color: EKColor(backgroundColor))
  
        attributes.positionConstraints.size = .init(
            width: .offset(value: 10),
            height: .ratio(value: 0.95)
        )
        
        return attributes
    }
    
    public static func normalWithSize(size: CGSize, backgroundColor: UIColor, screenInteraction: EKAttributes.UserInteraction = .dismiss) -> EKAttributes {
        var attributes: EKAttributes = defaultAttributes
        
        attributes.screenInteraction = screenInteraction
        attributes.entryBackground = .color(color: EKColor(backgroundColor))
        
        attributes.positionConstraints.maxSize = .init(
            width: .constant(value: 400),
            height: .ratio(value: 0.8)
        )
        
        attributes.positionConstraints.size = .init(
            width: .constant(value: size.width),
            height: .constant(value: size.height)
        )
        
        return attributes
    }
    
}
