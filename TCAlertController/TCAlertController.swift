//
//  TCAlertController.swift
//  TalentModal
//
//  Created by Peerasak Unsakon on 17/11/2563 BE.
//

import UIKit
import SwiftEntryKit

final public class TCAlertController {
    
    public enum AlertStyle {
        case normal
        case partial
    }
    
    public var primaryBackgroundColor: UIColor = .yellow
    public var primaryTextColor: UIColor = .black
    
    public static let shared: TCAlertController = {
        let instance = TCAlertController()
        return instance
    }()
    
    public static var bundle: Bundle {
        let podBundle = Bundle(for: TCAlertController.self)
        
        guard let bundleURL = podBundle.url(forResource: "TCAlertController", withExtension: "bundle") else {
            return Bundle.main
        }
        return Bundle(url: bundleURL)!
    }
    
    public static func message(title: String? = nil, details: String? = nil, image: UIImage? = nil, closeButtonText: String = "Close", style: AlertStyle = .normal, buttonStyle: CardActionButton.Style = .fill, isScreenInteractable: Bool = true) {
        TCAlertController.message(title: title, details: details, image: image, closeButtonText: closeButtonText, next: nil, style: style)
    }
    
    public static func message(title: String?, details: String?, image: UIImage?, closeButtonText: String = "Close", next: (() -> Void)?, style: AlertStyle = .normal, buttonStyle: CardActionButton.Style = .fill, isScreenInteractable: Bool = true) {
        
        let cancel = DialogActionButton(title: closeButtonText, style: buttonStyle, action: {
            SwiftEntryKit.dismiss()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel])
        let dialog = Dialog(image: image, title: title, detail: details, actionPanel: panels)
        
        
        var view: UIView!
        
        switch style {
        case .normal:
            view = CardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: nil, actions: dialog.actionPanel, footer: nil)
        case .partial:
            view = PartialCardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: nil, actions: dialog.actionPanel, footer: nil)
            break
        }
        let preset = isScreenInteractable ? ModalPreset.normal() : ModalPreset.normal(screenInteraction: .absorbTouches)
        display(entry: view, using: preset, onDisappear: next)
    }
    
    public static func message(title: String?, attributedDetails: NSAttributedString?, image: UIImage?, closeButtonText: String = "Close", isScreenInteractable: Bool = true, next: (() -> Void)?, style: AlertStyle = .normal, buttonStyle: CardActionButton.Style = .fill) {
        
        let cancel = DialogActionButton(title: closeButtonText, style: buttonStyle, action: {
            SwiftEntryKit.dismiss()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel])
        let dialog = Dialog(image: image, title: title, attributedDetail: attributedDetails, actionPanel: panels)
        
        var view: UIView!
        
        switch style {
        case .normal:
            view = CardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: nil, actions: dialog.actionPanel, footer: nil)
        case .partial:
            view = PartialCardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: nil, actions: dialog.actionPanel, footer: nil)
            break
        }
        
        let attribute: EKAttributes = isScreenInteractable ? ModalPreset.normal() : ModalPreset.normal(screenInteraction: .absorbTouches)
        
        display(entry: view, using: attribute, onDisappear: next)
    }
    
    public static func success(title: String? = nil, details: String? = nil, image: UIImage? = nil, closeButtonText: String = "Close", isScreenInteractable: Bool = true, next: (() -> Void)? = nil, style: AlertStyle = .normal) {
        
        let cancel = DialogActionButton(title: closeButtonText, style: .fill, action: {
            SwiftEntryKit.dismiss()
        })
        
        let titleColor: UIColor = UIColor(red: 0.576, green: 0.729, blue: 0.067, alpha: 1.00)
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel])
        
        let dialog = Dialog(image: image, title: title, detail: details, actionPanel: panels)
        
        var view: UIView!
        
        switch style {
        case .normal:
            let cardView = CardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = titleColor
            view = cardView
        case .partial:
            let cardView = PartialCardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = titleColor
            view = cardView
            break
        }
        
        let attribute: EKAttributes = isScreenInteractable ? ModalPreset.normal() : ModalPreset.normal(screenInteraction: .absorbTouches)
        
        display(entry: view, using: attribute, onDisappear: next)
    }
    
    public static func confirmation(title: String?, details: String?, image: UIImage?, confirmText: String = "Confirm", cancelText: String = "Cancel", style: AlertStyle = .normal, next: (() -> Void)?, dismiss: (() -> Void)? = nil) {
        
        let cancel = DialogActionButton(title: cancelText, style: .outlined, action: {
            SwiftEntryKit.dismiss()
            dismiss?()
        })
        
        let okay = DialogActionButton(title: confirmText, style: .fill, action: {
            SwiftEntryKit.dismiss()
            next?()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel, okay])
        let dialog = Dialog(image: image, title: title, detail: details, actionPanel: panels)
        
        var view: UIView!
        
        switch style {
        case .normal:
            view = CardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: image, actions: dialog.actionPanel, footer: nil)
        case .partial:
            view = PartialCardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: image, actions: dialog.actionPanel, footer: nil)
            break
        }
        
        display(entry: view, using: ModalPreset.normal(screenInteraction: .absorbTouches))
        
    }
    
    public static func confirmationWithAgreement(title: String?, details: String?, image: UIImage?, confirmText: String = "Confirm", cancelText: String = "Cancel", style: AlertStyle = .normal, showAgreement: (() -> Void)?, next: (() -> Void)?) {
        let cancel = DialogActionButton(title: cancelText, style: .outlined, action: {
            SwiftEntryKit.dismiss()
        })
        
        let confirm = DialogActionButton(title: confirmText, style: .fill, action: {
            SwiftEntryKit.dismiss()
            next?()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel, confirm])
        let dialog = Dialog(image: image, title: title, detail: details, actionPanel: panels, requiredAgreement: true, agreementHandler: showAgreement)
        
        var view: UIView!
        
        switch style {
        case .normal:
            view = CardDialogView(dialogInfo: dialog, footer: nil)
        case .partial:
            view = PartialCardDialogView(dialogInfo: dialog, footer: nil)
            break
        }

        display(entry: view, using: ModalPreset.normal(screenInteraction: .absorbTouches))
    }
    
    public static func error(_ error: Error, isScreenInteractable: Bool = true) {
        TCAlertController.error(title: "Oops!", details: error.localizedDescription, isScreenInteractable: isScreenInteractable, next: nil)
    }
    
    public static func error(_ error: Error, isScreenInteractable: Bool = true, next: (() -> Void)?) {
        TCAlertController.error(title: "Oops!", details: error.localizedDescription, isScreenInteractable: isScreenInteractable, next: next)
    }
    
    public static func error(title: String?, details: String?, image: UIImage? = nil, closeButtonText: String = "Close", isScreenInteractable: Bool = true, next: (() -> Void)?, style: AlertStyle = .normal) {
        
        let cancel = DialogActionButton(title: closeButtonText, style: .fill, action: {
            SwiftEntryKit.dismiss()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel])
        
        let dialog = Dialog(image: image, title: title, detail: details, actionPanel: panels)
        
        var view: UIView!
        
        switch style {
        case .normal:
            let cardView = CardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00)
            view = cardView
        case .partial:
            let cardView = PartialCardDialogView(title: dialog.title, subtitle: nil, details: dialog.detail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00)
            view = cardView
            break
        }
        
        let attribute: EKAttributes = isScreenInteractable ? ModalPreset.normal() : ModalPreset.normal(screenInteraction: .absorbTouches)
        
        display(entry: view, using: attribute, onDisappear: next)
    }
    
    public static func error(title: String?, attributedDetails: NSAttributedString?, image: UIImage? = nil, closeButtonText: String = "Close", isScreenInteractable: Bool = true, next: (() -> Void)?, style: AlertStyle = .normal) {
        
        let cancel = DialogActionButton(title: closeButtonText, style: .fill, action: {
            SwiftEntryKit.dismiss()
        })
        
        let panels = DialogActions(alignment: .horizontal, buttons: [cancel])
        
        let dialog = Dialog(image: image, title: title, attributedDetail: attributedDetails, actionPanel: panels)
        
        var view: UIView!
        
        switch style {
        case .normal:
            let cardView = CardDialogView(title: dialog.title, subtitle: nil, attributedDetails: dialog.attributedDetail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00)
            view = cardView
        case .partial:
            let cardView = PartialCardDialogView(title: dialog.title, subtitle: nil, attributedDetails: dialog.attributedDetail, image: dialog.image, actions: dialog.actionPanel, footer: nil)
            cardView.titleLabel.textColor = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00)
            view = cardView
            break
        }
        
        let attribute: EKAttributes = isScreenInteractable ? ModalPreset.normal() : ModalPreset.normal(screenInteraction: .absorbTouches)
        
        display(entry: view, using: attribute, onDisappear: next)
    }
    
    
    public static func show(viewController: UIViewController, windowSize: CGSize? = nil, screenInteraction: EKAttributes.UserInteraction = .dismiss, next: (() -> Void)? = nil, dismiss: (() -> Void)? = nil) {
        
        viewController.dialogDismissHandler = {
            SwiftEntryKit.dismiss()
            dismiss?()
        }
        
        viewController.dialogCompletionHandler = {
            SwiftEntryKit.dismiss()
            next?()
        }
        
        let backgroundColor = viewController.view.backgroundColor ?? .white
        viewController.view.backgroundColor = .clear
        
        var attribute: EKAttributes = ModalPreset.normal(screenInteraction: screenInteraction)
        
        if let size = windowSize {
            attribute = ModalPreset.normalWithSize(size: size, backgroundColor: backgroundColor, screenInteraction: screenInteraction)
        }
        
        display(entry: viewController, using: attribute, onDisappear: dismiss)
    }

}

fileprivate func display(entry: Any?, using: EKAttributes, onDisappear: (() -> Void)? = nil) {
    var attr = using
    attr.lifecycleEvents.willDisappear = {
        onDisappear?()
    }
    if let view = entry as? UIView {
        SwiftEntryKit.display(entry: view, using: attr, presentInsideKeyWindow: true)
    }
    if let viewController = entry as? UIViewController {
        SwiftEntryKit.display(entry: viewController, using: attr, presentInsideKeyWindow: true)
    }
}
