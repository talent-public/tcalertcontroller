//
//  ViewController.swift
//  TCAlertControllerExample
//
//  Created by Peerasak Unsakon on 9/12/2563 BE.
//

import UIKit
import TCAlertController

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        TCAlertController.shared.primaryBackgroundColor = .red
        TCAlertController.shared.primaryTextColor = .white
    }

    @IBAction func buttonTapped(_ sender: Any) {
        
    
        TCAlertController.message(title: "Hello", details: "Yes, I'm here", image: nil) {
            print("Hello")
        }
        
    }
    
}

